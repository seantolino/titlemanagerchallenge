# Title Manager Challenge

### How to run the application (it will be accessible at port 3000 on localhost):

`docker-compose up`

### How to run tests (this will run all tests, including integration tests):

1. `npm install` (node 10)
2. `docker-compose up mongo` (just start up the mongo service by itself; give it a few seconds)
3. `npm run test:docker`

##### Without integration tests (mongo not needed):
- `npm run test`

##### Watch mode:

- With integration tests (make sure to start mongo first with `docker-compose up mongo`): `npm run test:docker:watch`
- Without integration tests: `npm run test:watch`


### Endpoints

1. `GET /:type?page=<page>&limit=<limit>` - used for retrieving titles by type (paginated) 

    - Example 1: `curl http://localhost:3000/feature` (page and limit are optional; they default to 1 & 10, respectively)
    - Example 2: `curl http://localhost:3000/feature?page=1&limit=10`
    - Valid title type slugs are: `feature`, `bonus`, `tvSeries`, `episode`, `season`

2. `GET /:type/:id` - used for retrieving a single title by type and id

    - Example: `curl http://localhost:3000/feature/someObjectId`
    
3. `POST /:type` - used for creating a title with a particular type

    - Example: `curl -X POST -H "Content-Type: application/json" -d '{ "type": "Bonus", "name": "Star Wars" }' http://localhost:3000/bonus`

4. `PUT /:id` - used for updating an existing title with a particular id

    - Example: `curl -X PUT -H "Content-Type: application/json" -d '{ "name": "A better name!" }' http://localhost:3000/someObjectId`

5. `DELETE /:id` - used for deleting an existing title with a particular id

    - Example: `curl -X DELETE http://localhost:3000/someObjectId`
    
6. `GET /healthz` - health check

    - Example: `curl http://localhost:3000/healthz`

7. `GET /ready` - readiness check

    - Example: `curl http://localhost:3000/ready`

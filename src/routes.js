import logger from './logger';
import Router from 'koa-router';
import titleRoutes from 'routes/titleRoutes';

const router = new Router();
//istanbul ignore next
router.get('/healthz', (ctx) => {
    logger.info('GET /healthz');
    ctx.status = 200;
});
//istanbul ignore next
router.get('/ready', (ctx) => {
    logger.info('GET /ready');
    ctx.status = 200;
});
router.use(titleRoutes);

export default router.routes();

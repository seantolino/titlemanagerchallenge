import config from 'config';
import mongoose from 'mongoose';

const ObjectId = mongoose.Types.ObjectId;
const titleTypes = config.get('titleTypes').map(o => o.slug);

export function isValidId(id) {
    return ObjectId.isValid(id);
}

export function isValidType(type) {
    return titleTypes.includes(type);
}

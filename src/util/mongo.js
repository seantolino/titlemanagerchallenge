import config from 'config';
import mongoose from 'mongoose';

const mongoConfig = config.get('mongo');
const connectionString = `mongodb://${mongoConfig.get('username')}:${mongoConfig.get('password')}@${mongoConfig.get('host')}:${mongoConfig.get('port')}/titlemanager`;

export default async function connect() {
    await mongoose.connect(connectionString, {
        poolSize: mongoConfig.get('connectionPoolSize'),
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    });
}

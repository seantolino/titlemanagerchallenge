import Router from 'koa-router';
import titleController from 'controller/titleController';

const titleRouter = new Router();

titleRouter.get('/:type', (ctx) => titleController.filterByType(ctx));
titleRouter.get('/:type/:id', (ctx) => titleController.findById(ctx));
titleRouter.post('/:type', (ctx) => titleController.create(ctx));
titleRouter.put('/:id', (ctx) => titleController.updateById(ctx));
titleRouter.delete('/:id', (ctx) => titleController.deleteById(ctx));

export default titleRouter.routes();

import bluebird from 'bluebird';
import config from 'config';
import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

mongoose.Promise = bluebird;
const Schema = mongoose.Schema;

const defaultSchemaOptions = {
    toObject: {
        transform: function (doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            delete ret.__t;
        }
    }
};

const bonusTitleSchema = new Schema({
    duration: String,
    parentTitle: mongoose.ObjectId
}, defaultSchemaOptions);

const featureSchema = new Schema({
    theatricalReleaseDate: Date,
    duration: String
}, defaultSchemaOptions);

const episodeSchema = new Schema({
    releaseDate: Date,
    duration: String,
    parentTitle: mongoose.ObjectId
}, defaultSchemaOptions);

const seasonSchema = new Schema({
    releaseDate: Date,
    episodes: [episodeSchema],
    parentTitle: mongoose.ObjectId
}, defaultSchemaOptions);

const tvSeriesSchema = new Schema({
    releaseDate: Date,
    seasons: [seasonSchema],
    parentTitle: mongoose.ObjectId
}, defaultSchemaOptions);

const titleSchema = new Schema({
    name: String,
    description: String,
    bonuses: [bonusTitleSchema],
    type: {
        type: String,
        enum: [...config.get('titleTypes').map(o => o.type)],
        required: true
    }
}, defaultSchemaOptions);
titleSchema.plugin(mongoosePaginate);

titleSchema.statics.findOneById = async function (id) {
    const result = await this.findById(id);
    return result ? result.toObject() : null;
};

titleSchema.statics.createByType = async function (type, title) {
    const model = titleModels[type];
    const result = await model.create(title);
    return result ? result.toObject() : null;
};

titleSchema.statics.findByIdAndUpdate = async function (id, updates) {
    const result = await this.findOneAndUpdate({_id: id}, updates, {new: true});
    return result ? result.toObject() : null;
};

titleSchema.statics.deleteOneById = async function (id) {
    return await this.deleteOne({_id: id});
};

titleSchema.statics.filterByType = async function (type, page, limit) {
    const titleType = config.get('titleTypes').find(t => t.slug === type);
    const result = await this.paginate({type: titleType.type}, {page, limit});
    return result ? {
        ...result,
        docs: result.docs.map(doc => doc.toObject())
    } : null;
};

const Title = mongoose.models.Title || mongoose.model('Title', titleSchema);
const BonusTitle = (Title.discriminators && Title.discriminators.BonusTitle) || Title.discriminator(
    'BonusTitle',
    bonusTitleSchema
);

const Feature = (Title.discriminators && Title.discriminators.Feature) || Title.discriminator(
    'Feature',
    featureSchema
);

const Episode = (Title.discriminators && Title.discriminators.Episode) || Title.discriminator(
    'Episode',
    episodeSchema
);

const Season = (Title.discriminators && Title.discriminators.Season) || Title.discriminator(
    'Season',
    seasonSchema
);
const TvSeries = (Title.discriminators && Title.discriminators.TvSeries) || Title.discriminator(
    'TvSeries',
    tvSeriesSchema
);

const titleModels = {
    bonus: BonusTitle,
    feature: Feature,
    episode: Episode,
    season: Season,
    tvSeries: TvSeries
};

export default Title;

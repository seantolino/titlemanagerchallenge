import bodyParser from 'koa-bodyparser';
import config from 'config';
import connect from 'util/mongo';
import helmet from 'koa-helmet';
import http from 'http';
import Koa from 'koa';
import routes from './routes';

const app = new Koa();
const initServices = [connect];

app.use(helmet());
app.use(bodyParser());
app.use(routes);

export async function start() {
    await Promise.all(initServices.map(s => s()));
    const server = http.createServer(app.callback());
    const appConfig = config.get('app');

    server.listen(appConfig.port, appConfig.host);
    return server;
}

import {isValidId, isValidType} from 'util/assert';
import logger from 'logger';
import titleService from 'service/titleService';
import _ from 'lodash';

async function filterByType(ctx) {
    const type = ctx.params.type;
    const page = ctx.query.page ? Number(ctx.query.page) : 1;
    const limit = ctx.query.limit ? Number(ctx.query.limit) : 10;
    ctx.assert(isValidType(type), 'Provided type is invalid', 400);
    ctx.assert(_.isFinite(page) && _.isFinite(limit), 'Page and limit must be positive numbers.', 400);

    ctx.body = await titleService.filterByType(type, page, limit);
}

async function findById(ctx) {
    const id = ctx.params.id;
    ctx.assert(isValidId(id), 'Provided ID is invalid.', 400);

    ctx.body = await titleService.findById(id);
}

async function updateById(ctx) {
    const id = ctx.params.id;
    const titleUpdates = ctx.request.body;
    ctx.assert(isValidId(id), 'Provided ID is invalid.', 400);

    try {
        ctx.body = await titleService.updateById(id, titleUpdates);
    } catch (e) {
        ctx.status = 400;
        ctx.body = 'Invalid title update. Make sure that all included fields being updated are valid.';
        logger.error(e);
    }
}

async function deleteById(ctx) {
    const id = ctx.params.id;
    ctx.assert(isValidId(id), 'Provided ID is invalid.', 400);

    ctx.body = await titleService.deleteById(id);
}

async function create(ctx) {
    const type = ctx.params.type;
    const title = ctx.request.body;
    ctx.assert(isValidType(type), 'Provided title type is invalid.', 400);

    try {
        ctx.body = await titleService.create(type, title);
    } catch (e) {
        ctx.status = 400;
        ctx.body = 'Invalid title. Make sure your title includes a valid type (required) and that all fields are valid.';
        logger.error(e);
    }
}

export default {
    filterByType,
    findById,
    updateById,
    deleteById,
    create
};

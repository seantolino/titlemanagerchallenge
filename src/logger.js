import winston from 'winston';

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: {service: 'title-manager'},
    transports: [
        new winston.transports.Console({level: 'info'}),
    ]
});

export default logger;

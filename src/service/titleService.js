import Title from 'model/title';

async function filterByType(type, page, limit) {
    return Title.filterByType(type, page, limit);
}

async function findById(id) {
    return Title.findOneById(id);
}

async function updateById(id, titleUpdates) {
    return Title.findByIdAndUpdate(id, titleUpdates);
}

async function deleteById(id) {
    return Title.deleteOneById(id);
}

async function create(type, title) {
    return Title.createByType(type, title);
}

export default {
    filterByType,
    findById,
    updateById,
    deleteById,
    create
};

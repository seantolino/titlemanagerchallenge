import {getKoaTestCtx} from '../util/koaTestContext';
import chai from 'chai';
import mockTitleService from '../mocks/mockTitleService';
import proxyquire from 'proxyquire';
import sinon from 'sinon';
import _ from 'lodash';

const expect = chai.expect;

describe('Title Controller', () => {

    let titleController = null;
    let titleServiceMock = null;
    let sandbox = null;
    before(() => {
        titleController = proxyquire.noCallThru().load('../../src/controller/titleController.js', {
            'service/titleService': mockTitleService,
            'util/assert': {
                isValidId: _.noop,
                isValidType: _.noop
            }
        }).default;
        sandbox = sinon.createSandbox({});
    });

    beforeEach(() => {
        titleServiceMock = sandbox.mock(mockTitleService);
    });

    afterEach(() => {
        titleServiceMock.verify();
        titleServiceMock.restore();
    });

    describe('filterByType', () => {
        it('should call the filterByType method of the title service with the provided type, page and limit', async () => {
            const koaTestCtx = getKoaTestCtx();
            const type = 'random type';
            koaTestCtx.params.type = type;
            koaTestCtx.assert = sandbox.stub();

            titleServiceMock
                .expects('filterByType')
                .withArgs(type)
                .once();

            await titleController.filterByType(koaTestCtx);
            sandbox.assert.calledTwice(koaTestCtx.assert);
        });
    });

    describe('findById', () => {
        it('should call the findById method of the title service with the provided id', async () => {
            const koaTestCtx = getKoaTestCtx();
            const id = 'randomObjectId';
            koaTestCtx.params.id = id;
            koaTestCtx.assert = sandbox.stub();

            titleServiceMock
                .expects('findById')
                .withArgs(id)
                .once();

            await titleController.findById(koaTestCtx);
            sandbox.assert.calledOnce(koaTestCtx.assert);
        });
    });

    describe('updateById', () => {
        it('should call the updateById method of the title service with the provided id and updates', async () => {
            const koaTestCtx = getKoaTestCtx();
            const id = 'randomObjectId';
            koaTestCtx.params.id = id;
            koaTestCtx.assert = sandbox.stub();
            koaTestCtx.request.body = {};

            titleServiceMock
                .expects('updateById')
                .withArgs(id, koaTestCtx.request.body)
                .once();

            await titleController.updateById(koaTestCtx);
            sandbox.assert.calledOnce(koaTestCtx.assert);
        });

        it('should catch errors and return a 400 status code (bad request)', async () => {
            const koaTestCtx = getKoaTestCtx();
            const id = 'randomObjectId2';
            koaTestCtx.params.id = id;
            koaTestCtx.assert = sandbox.stub();
            koaTestCtx.request.body = {};

            titleServiceMock
                .expects('updateById')
                .withArgs(id, koaTestCtx.request.body)
                .once()
                .throws();

            await titleController.updateById(koaTestCtx);
            sandbox.assert.calledOnce(koaTestCtx.assert);
            expect(koaTestCtx.status).to.equal(400);
        });
    });

    describe('deleteById', () => {
        it('should call the deleteById method of the title service with the provided id', async () => {
            const koaTestCtx = getKoaTestCtx();
            const id = 'randomObjectId';
            koaTestCtx.params.id = id;
            koaTestCtx.assert = sandbox.stub();

            titleServiceMock
                .expects('deleteById')
                .withArgs(id)
                .once();

            await titleController.deleteById(koaTestCtx);
            sandbox.assert.calledOnce(koaTestCtx.assert);
        });
    });

    describe('create', () => {
        it('should call the create method of the title service with the provided type and title object', async () => {
            const koaTestCtx = getKoaTestCtx();
            const type = 'random title type';
            koaTestCtx.params.type = type;
            koaTestCtx.request.body = {};
            koaTestCtx.assert = sandbox.stub();

            titleServiceMock
                .expects('create')
                .withArgs(type, koaTestCtx.request.body)
                .once();

            await titleController.create(koaTestCtx);
            sandbox.assert.calledOnce(koaTestCtx.assert);
        });

        it('should catch errors and return a 400 status code (bad request)', async () => {
            const koaTestCtx = getKoaTestCtx();
            const type = 'random title type 2';
            koaTestCtx.params.type = type;
            koaTestCtx.request.body = {};
            koaTestCtx.assert = sandbox.stub();

            titleServiceMock
                .expects('create')
                .withArgs(type, koaTestCtx.request.body)
                .once()
                .throws();

            await titleController.create(koaTestCtx);
            sandbox.assert.calledOnce(koaTestCtx.assert);
            expect(koaTestCtx.status).to.equal(400);
        });
    });
});

import mockTitleModel from '../mocks/mockTitleModel';
import proxyquire from 'proxyquire';
import sinon from 'sinon';

describe('Title Service', () => {

    let titleService = null;
    let titleModelMock = null;
    let sandbox = null;
    before(() => {
        titleService = proxyquire.noCallThru().load('../../src/service/titleService.js', {
            'model/title': mockTitleModel,
        }).default;
        sandbox = sinon.createSandbox({});
    });

    beforeEach(() => {
        titleModelMock = sandbox.mock(mockTitleModel);
    });

    afterEach(() => {
        titleModelMock.verify();
        titleModelMock.restore();
    });

    describe('filterByType', () => {
        it('should call the filterByType method of the title model with the provided type, page and limit', async () => {
            const type = 'random type';
            const page = 1;
            const limit = 10;

            titleModelMock
                .expects('filterByType')
                .withArgs(type, page, limit)
                .once();

            await titleService.filterByType(type, page, limit);
        });
    });

    describe('findOneById', () => {
        it('should call the findOneById method of the title model with the provided id', async () => {
            const id = 'randomObjectId';

            titleModelMock
                .expects('findOneById')
                .withArgs(id)
                .once();

            await titleService.findById(id);
        });
    });

    describe('findByIdAndUpdate', () => {
        it('should call the findByIdAndUpdate method of the title model with the provided id and updates', async () => {
            const id = 'randomObjectId';
            const titleUpdates = {};

            titleModelMock
                .expects('findByIdAndUpdate')
                .withArgs(id, titleUpdates)
                .once();

            await titleService.updateById(id, titleUpdates);
        });
    });

    describe('deleteOneById', () => {
        it('should call the deleteOneById method of the title model with the provided id', async () => {
            const id = 'randomObjectId';

            titleModelMock
                .expects('deleteOneById')
                .withArgs(id)
                .once();

            await titleService.deleteById(id);
        });
    });

    describe('createByType', () => {
        it('should call the createByType method of the title model with the provided type and title object', async () => {
            const type = 'random title type';
            const title = {};

            titleModelMock
                .expects('createByType')
                .withArgs(type, title)
                .once();

            await titleService.create(type, title);
        });
    });
});

import _ from 'lodash';

export function getKoaTestCtx() {
    return {
        params: {},
        query: {},
        assert: _.noop,
        request: {
            body: {}
        }
    };
}

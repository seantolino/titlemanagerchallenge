import {start} from 'app';
import chai from 'chai';
import config from 'config';
import mongoose from 'mongoose';
import request from 'superagent';
import titles from './titles';
import testRequest from 'supertest';
import Title from 'model/title';

const expect = chai.expect;
const hostPrefix = `http://localhost:${config.get('app').port}`;
describe('Title Manager API Integration Tests', () => {
    const titleTypes = config.get('titleTypes');
    let server = null;
    before(function () {
        if (process.env.NODE_ENV !== 'test-docker') {
            this.skip();
        }
    });

    beforeEach(async () => {
        server = await start();
        await Promise.all(titles.map(title => {
            const titleTypeSlug = titleTypes.find(t => t.type === title.type).slug;
            return request.post(`${hostPrefix}/${titleTypeSlug}`).send(title);
        }));
    });

    afterEach(async () => {
        await server.close();
        await Title.remove({});
    });

    describe('filterByType', () => {
        it('should return results filtered by type (Feature)', async () => {
            const response = await testRequest(server).get('/feature').expect('Content-Type', /json/).expect(200);
            const docs = response.body.docs;
            expect(docs.length).to.not.equal(0);
            docs.forEach(doc => expect(doc.type).to.equal('Feature'));
        });

        it('should return results filtered by type (TV Series)', async () => {
            const response = await testRequest(server).get('/tvSeries').expect('Content-Type', /json/).expect(200);
            const docs = response.body.docs;
            expect(docs.length).to.not.equal(0);
            docs.forEach(doc => expect(doc.type).to.equal('TV Series'));
        });

        it('should return result fitereld by type (Feature) and specified page, limit', async () => {
            const response = await testRequest(server).get('/feature?page=2&limit=1').expect('Content-Type', /json/).expect(200);
            const docs = response.body.docs;
            expect(docs.length).to.equal(1);
            docs.forEach(doc => expect(doc.type).to.equal('Feature'));
        });

        it('should return a 400 status code (bad request) for an invalid title type', async () => {
            await testRequest(server).get('/random').expect(400);
        });
    });

    describe('findById', () => {
        it('should return a title by id if it exists', async () => {
            const createResponse = await request.post(`${hostPrefix}/feature`).send({
                type: 'Feature',
                name: 'Mickey Mouse'
            });
            const id = createResponse.body.id;
            const findByIdResponse = await testRequest(server).get(`/feature/${id}`).expect('Content-Type', /json/).expect(200);
            expect(findByIdResponse.body.id).to.equal(id);
        });

        it('should return a 400 status code (bad request) if the provided id is not a valid ObjectId', async () => {
            const id = 'randomInvalidId';
            await testRequest(server).get(`/feature/${id}`).expect(400);
        });

        it('should not return a 204 status code (no content) if there is no title with the provided id', async () => {
            const id = mongoose.Types.ObjectId();
            await testRequest(server).get(`/feature/${id}`).expect(204);
        });
    });

    describe('create', () => {
        it('should successfully create a title with the given type and data', async () => {
            const title = {
                type: 'Bonus',
                name: 'Really Cool Bonus Title!'
            };
            const createResponse = await testRequest(server).post(`/bonus`).send(title).expect(200);
            expect(createResponse.body.type).to.equal(title.type);
            expect(createResponse.body.name).to.equal(title.name);
        });

        it('should return a 400 status code (bad request) if the provided type is invalid', async () => {
            const title = {
                type: 'Bonus',
                name: 'Really Cool Bonus Title!'
            };
            await testRequest(server).post(`/invalidTitleType`).send(title).expect(400);
        });
    });

    describe('updateById', () => {
        it('should successfully update a title with the given data, if a title with the provided id exists', async () => {
            const title = {
                type: 'TV Series',
                name: 'The Greatest Name!'
            };
            const titleUpdates = {
                name: 'A better name than before!'
            };
            const createResponse = await request.post(`${hostPrefix}/tvSeries`).send(title);
            const id = createResponse.body.id;
            await testRequest(server).put(`/${id}`).send(titleUpdates).expect(200);
            const updatedDoc = await testRequest(server).get(`/tvSeries/${id}`).expect(200);
            expect(updatedDoc.body.name).to.equal(titleUpdates.name);
        });
    });

    describe('deleteById', () => {
        it('should successfully delete a title by id, if it exists', async () => {
            const title = {
                type: 'TV Series',
                name: 'The Greatest Name!'
            };
            const createResponse = await request.post(`${hostPrefix}/tvSeries`).send(title);
            const id = createResponse.body.id;
            const response = await testRequest(server).delete(`/${id}`).expect(200);
            expect(response.body.deletedCount).to.equal(1);
        });
    });
});

import _ from 'lodash';

export default {
    filterByType: _.noop,
    findById: _.noop,
    updateById: _.noop,
    deleteById: _.noop,
    create: _.noop
};

import _ from 'lodash';

export default {
    filterByType: _.noop,
    findOneById: _.noop,
    findByIdAndUpdate: _.noop,
    deleteOneById: _.noop,
    createByType: _.noop
};
